import os
from dotenv import load_dotenv

load_dotenv()


def load_env():
    return {
        'token': os.getenv("TGTOKEN"),
        'chat_id': os.getenv("TGCHATID")
    }