import os

def load_texts(path='./txts'):
    txts = {}
    for name in os.listdir(path):
        with open(f'{path}/{name}', 'r') as txt:
            txts[name] = txt.read()
    
    return txts