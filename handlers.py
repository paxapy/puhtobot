from telegram import Update
from telegram.ext import CallbackContext

from txt_loader import load_texts
from env_loader import load_env


env = load_env()
txts = load_texts()

## command handlers

def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""

    update.message.reply_text(txts['start'])


def nomercy(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text(txts['help'])


def what(update: Update, context: CallbackContext) -> None:
    """Send a message about what."""
    update.message.reply_text(txts['what'])


def where(update: Update, context: CallbackContext) -> None:
    """Send a message about where."""
    update.message.reply_text(txts['where'])


def how(update: Update, context: CallbackContext) -> None:
    """Send a message about how."""
    update.message.reply_text(txts['how'])

## message handlers

def yo(update: Update, context: CallbackContext) -> None:
    """Echo the user message."""
    update.message.forward(env['chat_id'])

def reply(update: Update, context: CallbackContext):
    """Reply to user as bot."""
    user_id = update.message.reply_to_message.forward_from.id
    context.bot.copy_message(
        message_id=update.message.message_id,
        chat_id=user_id,
        from_chat_id=update.message.chat_id
    )
